# README #

Использование контроллера конвертации валюты

### Demo ###

* [Demo](http://tanuki.aferon.com/)

### Usage ###
```php
<?php
include "module/curse/bootstrap.php";

$from = "usd";
$to = "euro";
$amount = "2"; //2 usd

$curse = new Module\Curse\Controller;
$curse->fromCurrency = $from;
$value = $curse->get($to, $amount);

echo $amount . " " . $from . " = " . $value . " " . $to;

```