<?php

namespace Module\Curse;

class Exception extends \Exception {}

class logController
{
	public $logfile;
	function __construct($str)
	{
		$this->logfile = "curse_log.txt";
		
		if (!is_writable($this->logfile)) 
			die("Error write log file");
			
		file_put_contents($this->logfile, $str . "\r\n", FILE_APPEND);
	}

}

class cache {
	private $store;
	
	function __construct()
	{
		$this->store["rub"] = 30;
		$this->store["tenge"] = 5;
	}
	public function get($currency)
	{
		if(!$this->store[$currency])
			throw new Exception("Cache " . $currency . ' not found');

		return $this->store[$currency];
	}
	
	public function set($key, $value)
	{
		$this->store[$key] = $value;
	}
	
	
};

class db {
	private $store;
	
	function __construct()
	{
		$this->store["rub"] = 30;
		$this->store["euro"] = 0.8;
	}
	public function get($currency)
	{
		if(!$this->store[$currency])
			throw new Exception("Db " . $currency . ' not found');

		return $this->store[$currency];
	}
	
	public function set($key, $value)
	{
		$this->store[$key] = $value;
	}
};

class remote {
	public function get()
	{
		$json = json_decode("{ usd: 1, rub: 30.0, euro, 0.8, tenge, 6}");
		
		if(!$this->store[$currency])
			throw new Exception("Remote " . $currency . ' not found or terminate');
			
		return $this->store[$currency];
	}
};

class Controller
{
	public $fromCurrency;
	
	function __construct()
	{
		$this->fromCurrency = "usd";
	}
	
	function get($currency, $amount)
	{
		$cache = new cache();
		try
		{
			return $cache->get($currency) * $amount;
		} 
		catch (Exception $e)
		{
			new logController($e);
			
			$db = new db();
			try
			{
				$value = $db->get($currency);
				$cache->set($currency, $value);
				return $value * $amount;
			} 
			catch (Exception $e)
			{
				new logController($e);
				
				$remote = new remote();
				try
				{
					$value = $remote->get($currency);
					$cache->set($currency, $value);
					$db->set($currency, $value);
					return $value * $amount;
				}
				catch (Exception $e)
				{
					new logController($e);
					
					die("service error curse controller not work");
				}
			}
			
		}
		
		return $result;
	}
}