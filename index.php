<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL ^ E_NOTICE);

include "module/curse/bootstrap.php";

$from = "usd";
$to = "euro";
$amount = "2"; //2 usd

$curse = new Module\Curse\Controller;
$curse->fromCurrency = $from;
$value = $curse->get($to, $amount);

echo $amount . " " . $from . " = " . $value . " " . $to;